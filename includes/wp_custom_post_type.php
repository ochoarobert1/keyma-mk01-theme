<?php

// Register Custom Post Type
function clientes() {

    $labels = array(
        'name'                  => _x( 'Clientes', 'Post Type General Name', 'keyma' ),
        'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'keyma' ),
        'menu_name'             => __( 'Clientes', 'keyma' ),
        'name_admin_bar'        => __( 'Cliente', 'keyma' ),
        'archives'              => __( 'Archivo de Clientes', 'keyma' ),
        'attributes'            => __( 'Atributos de Cliente', 'keyma' ),
        'parent_item_colon'     => __( 'Cliente Padre:', 'keyma' ),
        'all_items'             => __( 'Todos los Clientes', 'keyma' ),
        'add_new_item'          => __( 'Agregar Nuevo Cliente', 'keyma' ),
        'add_new'               => __( 'Agregar Nuevo', 'keyma' ),
        'new_item'              => __( 'Nuevo Cliente', 'keyma' ),
        'edit_item'             => __( 'Editar Cliente', 'keyma' ),
        'update_item'           => __( 'Actualizar Cliente', 'keyma' ),
        'view_item'             => __( 'Ver Cliente', 'keyma' ),
        'view_items'            => __( 'Ver Clientes', 'keyma' ),
        'search_items'          => __( 'Buscar Cliente', 'keyma' ),
        'not_found'             => __( 'No hay resultados', 'keyma' ),
        'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'keyma' ),
        'featured_image'        => __( 'Logo del Cliente', 'keyma' ),
        'set_featured_image'    => __( 'Colocar Logo de Cliente', 'keyma' ),
        'remove_featured_image' => __( 'Remover Logo de Cliente', 'keyma' ),
        'use_featured_image'    => __( 'Usar como Logo de Cliente', 'keyma' ),
        'insert_into_item'      => __( 'Insertar en Cliente', 'keyma' ),
        'uploaded_to_this_item' => __( 'Cargado a este Cliente', 'keyma' ),
        'items_list'            => __( 'Listado de Clientes', 'keyma' ),
        'items_list_navigation' => __( 'Navegación del Listado de Clientes', 'keyma' ),
        'filter_items_list'     => __( 'Filtro del Listado de Clientes', 'keyma' ),
    );
    $args = array(
        'label'                 => __( 'Cliente', 'keyma' ),
        'description'           => __( 'Clientes de Keyma', 'keyma' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'thumbnail', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-groups',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,		
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'show_in_rest'          => false,
    );
    register_post_type( 'clientes', $args );

}
add_action( 'init', 'clientes', 0 );

// Register Custom Post Type
function servicios() {

	$labels = array(
		'name'                  => _x( 'Servicios', 'Post Type General Name', 'keyma' ),
		'singular_name'         => _x( 'Servicio', 'Post Type Singular Name', 'keyma' ),
		'menu_name'             => __( 'Servicios', 'keyma' ),
		'name_admin_bar'        => __( 'Servicios', 'keyma' ),
		'archives'              => __( 'Archivo de Servicios', 'keyma' ),
		'attributes'            => __( 'Atributos de Servicio', 'keyma' ),
		'parent_item_colon'     => __( 'Servicio Padre:', 'keyma' ),
		'all_items'             => __( 'Todos los Servicios', 'keyma' ),
		'add_new_item'          => __( 'Agregar Nuevo Servicio', 'keyma' ),
		'add_new'               => __( 'Agregar Nuevo', 'keyma' ),
		'new_item'              => __( 'Nuevo Servicio', 'keyma' ),
		'edit_item'             => __( 'Editar Servicio', 'keyma' ),
		'update_item'           => __( 'Actualizar Servicio', 'keyma' ),
		'view_item'             => __( 'Ver Servicio', 'keyma' ),
		'view_items'            => __( 'Ver Servicios', 'keyma' ),
		'search_items'          => __( 'Buscar Servicio', 'keyma' ),
		'not_found'             => __( 'No hay resultados', 'keyma' ),
		'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'keyma' ),
		'featured_image'        => __( 'Imagen del Servicio', 'keyma' ),
		'set_featured_image'    => __( 'Colocar Imagen del Servicio', 'keyma' ),
		'remove_featured_image' => __( 'Remover Imagen del Servicio', 'keyma' ),
		'use_featured_image'    => __( 'Usar como Imagen del Servicio', 'keyma' ),
		'insert_into_item'      => __( 'Insertar en Servicio', 'keyma' ),
		'uploaded_to_this_item' => __( 'Cargado a este Servicio', 'keyma' ),
		'items_list'            => __( 'Listado de Servicios', 'keyma' ),
		'items_list_navigation' => __( 'Navegación del Listado de Servicios', 'keyma' ),
		'filter_items_list'     => __( 'Filtro del Listado de Servicios', 'keyma' ),
	);
	$args = array(
		'label'                 => __( 'Servicio', 'keyma' ),
		'description'           => __( 'Servicios prestados en la empresa', 'keyma' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-grid-view',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => false,
	);
	register_post_type( 'servicios', $args );

}
add_action( 'init', 'servicios', 0 );

?>
