<?php
function special_menu_container($menu_special) {
?>
<ul class="main-nav-container">

    <?php $arrayitems = wp_get_menu_array($menu_special); ?>
    <?php $i = 1; ?>
    <?php foreach ($arrayitems as $items) {?>
    <?php $array_posts = array(); ?>
    <li class="nav_main_<?php echo $i; ?>" id="<?php echo $i; ?>">
        <?php $item_term = $items['url']; ?>
        <?php $variable = explode('/categoria-producto/', $item_term); ?>
        <?php $item_var = $variable[1]; ?>
        <?php $term_father = get_term_by( 'slug',  strstr($item_var, '/', true), 'product_cat'); ?>
        <?php if (!empty($term_father)) { ?>
        <a class="submenu-link" href="<?php echo $items['url']; ?>">
            <?php echo $items['title']; ?>
        </a>
        <?php if ($term_father->name === 'lockers') { ?>
        <ul id="dropdown-custom-<?php echo $i; ?>" class="dropdown-custom">
            <?php $terms = get_terms('product_cat', array('parent' => $term_father->term_id, 'orderby' => 'slug', 'hide_empty' => false)); ?>
            <?php foreach ($terms as $term) { ?>
            <li>
                <a href="<?php echo get_term_link( $term->name, 'product_cat'); ?>"><?php echo $term->name; ?></a>
            </li>
            <?php } ?>
        </ul>
        <?php } else { ?>
        <ul id="dropdown-custom-<?php echo $i; ?>" class="dropdown-custom">
            <li class="dropdown-custom-item col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <h2><?php _e('Nuevos Productos', 'keyma'); ?></h2>
                <?php $args = array('post_type' => 'product', 'posts_per_page' => 3, 'order' => 'DESC', 'orderby' => 'date', 'tax_query' => array( array( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => $term_father->slug ), ), ); ?>
                <?php query_posts($args); ?>
                <?php while (have_posts()) : the_post(); ?>
                <?php array_push($array_posts, get_the_ID()); ?>
                <?php $product = new WC_Product(get_the_ID()); ?>
                <div class="dropdown-custom-item-product col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <div class="media-custom col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <div class="media-img col-lg-3 col-md-3 col-sm-3 col-xs-3 no-paddingl no-paddingr">
                            <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                <picture>
                                    <?php the_post_thumbnail('product_menu', array('class' => 'img-responsive')); ?>
                                </picture>
                            </a>
                        </div>
                        <div class="media-info col-lg-9 col-md-9 col-sm-9 col-xs-9 no-paddingr">
                            <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                <h4><?php the_title(); ?></h4>
                                <div class="product-price col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <?php echo $product->get_price_html(); ?>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php wp_reset_query(); ?>
            </li>
            <li class="dropdown-custom-item col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <h2><?php _e('Productos Destacados', 'keyma'); ?></h2>
                <?php $args = array('post_type' => 'product', 'posts_per_page' => 3, 'order' => 'DESC', 'orderby' => 'date', 'post__not_in' => $array_posts, 'tax_query' => array( array( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => $term_father->slug ), ), ); ?>
                <?php query_posts($args); ?>
                <?php while (have_posts()) : the_post(); ?>
                <?php array_push($array_posts, get_the_ID()); ?>
                <?php $product = new WC_Product(get_the_ID()); ?>
                <div class="dropdown-custom-item-product col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <div class="media-custom col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <div class="media-img col-lg-3 col-md-3 col-sm-3 col-xs-3 no-paddingl no-paddingr">
                            <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                <picture>
                                    <?php the_post_thumbnail('product_menu', array('class' => 'img-responsive')); ?>
                                </picture>
                            </a>
                        </div>
                        <div class="media-info col-lg-9 col-md-9 col-sm-9 col-xs-9 no-paddingr">
                            <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                <h4><?php the_title(); ?></h4>
                                <div class="product-price col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <?php echo $product->get_price_html(); ?>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php wp_reset_query(); ?>
            </li>
            <li class="dropdown-custom-item col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <h2><?php _e('Productos + Visitados', 'keyma'); ?></h2>
                <?php $args = array('post_type' => 'product', 'posts_per_page' => 3, 'order' => 'DESC', 'orderby' => 'date', 'post__not_in' => $array_posts, 'tax_query' => array( array( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => $term_father->slug ), ), ); ?>
                <?php query_posts($args); ?>
                <?php while (have_posts()) : the_post(); ?>
                <?php array_push($array_posts, get_the_ID()); ?>
                <?php $product = new WC_Product(get_the_ID()); ?>
                <div class="dropdown-custom-item-product col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <div class="media-custom col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <div class="media-img col-lg-3 col-md-3 col-sm-3 col-xs-3 no-paddingl no-paddingr">
                            <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                <picture>
                                    <?php the_post_thumbnail('product_menu', array('class' => 'img-responsive')); ?>
                                </picture>
                            </a>
                        </div>
                        <div class="media-info col-lg-9 col-md-9 col-sm-9 col-xs-9 no-paddingr">
                            <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                <h4><?php the_title(); ?></h4>
                                <div class="product-price col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <?php echo $product->get_price_html(); ?>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php wp_reset_query(); ?>
            </li>
            <li class="dropdown-custom-item col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <h2><?php _e('Producto de la Semana', 'keyma'); ?></h2>
                <?php $args = array('post_type' => 'product', 'posts_per_page' => 1, 'tax_query' => array( array( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => $term_father->slug ), ), ); ?>
                <?php query_posts($args); ?>
                <?php while (have_posts()) : the_post(); ?>
                <?php $product = new WC_Product(get_the_ID()); ?>
                <div class="dropdown-custom-item-featured-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                        <picture>
                            <?php the_post_thumbnail('product_menu', array('class' => 'img-responsive')); ?>
                        </picture>
                    </a>
                    <h4><?php the_title(); ?></h4>
                    <div class="product-price col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <?php echo $product->get_price_html(); ?>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php wp_reset_query(); ?>
            </li>
        </ul>
        <?php } ?>
        <?php } else { ?>
        <a href="<?php echo $items['url']; ?>" class="submenu-link">
            <?php echo $items['title']; ?>
        </a>
        <?php } ?>
    </li>
    <?php $i++; } ?>
</ul>
<?php
                                               }
