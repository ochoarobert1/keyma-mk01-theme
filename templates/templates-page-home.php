<?php
/**
 * Template Name: Plantilla para Pagina de Inicio
 *
 * @package Keyma
 * @subpackage keyma-mk01-theme
 * @since 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php /* MAIN SLIDER - BUILT BY REVOLUTION SLIDER */?>
        <section class="the-slider col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <?php $slider = get_post_meta(get_the_ID(), 'rw_slider', true); ?>
            <?php echo do_shortcode('[rev_slider alias="' . $slider . '"]'); ?>
        </section>
        <?php /* MAIN FEATURED PRODUCTS */?>
        <?php $cats = get_post_meta(get_the_ID(), 'rw_home_cat', true); ?>
        <?php foreach ($cats as $item) { ?>
        <?php $product_cat = get_term_by('id', $item, 'product_cat'); ?>
        <?php include(locate_template('templates/templates-featured-products.php')); ?>
        <?php } ?>

        <!-- CLIENTES SECTION -->
        <section class="clients-section col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="clients-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2 class="section-title"><?php _e('Algunos de nuestros clientes', 'keyma'); ?></h2>
                        <hr>
                        <div class="client-slider col-lg-12 col-md-12 col-sm-12 col-xs-12 owl-carousel owl-theme">
                            <?php $args = array('post_type' => 'clientes', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date'); ?>
                            <?php query_posts($args); ?>
                            <?php while (have_posts()) : the_post(); ?>
                            <div class="client-item">
                                <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                            </div>
                            <?php endwhile; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- HERO SECTION -->
        <section class="hero-section col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <?php get_template_part('templates/map'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
