<?php
$coord = '-33.4545805,-70.6936757';
?>

<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyD-FW49_GyRnOVC4any4nf0IU1xyYqzDuY"></script>
<script type="text/javascript">

    var map = null
    var marker = null

    jQuery(document).ready(function() {
        var mapOptions = {
            center: new google.maps.LatLng(-33.454574, -70.691465),
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            navigationControl: false,
            scaleControl: false,
            optimized: false,
            zIndex: 0,
            /* HYBRID | ROADMAP | SATELLITE| TERRAIN */
            styles: [
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#7fc8ed"
                        },
                        {
                            "saturation": 55
                        },
                        {
                            "lightness": -6
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "hue": "#7fc8ed"
                        },
                        {
                            "saturation": 55
                        },
                        {
                            "lightness": -6
                        },
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "hue": "#83cead"
                        },
                        {
                            "saturation": 1
                        },
                        {
                            "lightness": -15
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "hue": "#f3f4f4"
                        },
                        {
                            "saturation": -84
                        },
                        {
                            "lightness": 59
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "hue": "#ffffff"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 100
                        },
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "hue": "#ffffff"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 100
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "hue": "#bbbbbb"
                        },
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 26
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "hue": "#ffcc00"
                        },
                        {
                            "saturation": 100
                        },
                        {
                            "lightness": -35
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "hue": "#ffcc00"
                        },
                        {
                            "saturation": 100
                        },
                        {
                            "lightness": -22
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "poi.school",
                    "elementType": "all",
                    "stylers": [
                        {
                            "hue": "#d7e4e4"
                        },
                        {
                            "saturation": -60
                        },
                        {
                            "lightness": 23
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                }
            ]
        };

        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(-33.454576, -70.691373),
            title: 'Keyma Corp Spa',
            map: map,
            icon: "<?php echo get_template_directory_uri(); ?>/images/marker-map.png"
        });

    });
</script>
<div class="clearfix"></div>
<div class="map">
    <div id="map_canvas" class="map_gray" style="width:100%; height:500px;"></div>
</div>
<div class="clearfix"></div>
