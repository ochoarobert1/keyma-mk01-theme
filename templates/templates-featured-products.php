<?php $array_posts = array(); ?>
<section class="featured-products col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
    <div class="container">
        <div class="row">
            <div class="featured-products-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="featured-product-title col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2><?php echo $product_cat->name; ?></h2>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <?php $args = array('post_type' => 'product', 'posts_per_page' => 5, 'order' => 'DESC', 'orderby' => 'date', 'tax_query' => array( array( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => $product_cat->slug ))); ?>
                    <?php query_posts($args); ?>
                    <?php if (have_posts()) : ?>
                    <div class="product-home-item-container col-lg-6 col-md-6 col-sm-6 col-xs-12 no-paddingl">
                        <div class="product-home-item-container-wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl">
                            <?php while (have_posts()) : the_post(); ?>
                            <?php array_push($array_posts, get_the_ID());  ?>
                            <?php $product = new WC_Product(get_the_ID()); ?>
                            <div class="product-home-item col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                <div class="product-home-item-img col-lg-4 col-md-4 col-sm-4 col-xs-4 no-paddingl no-paddingr">
                                    <a href="<?php the_permalink(); ?>" title="<?php _e('Ver Producto', 'keyma'); ?>">
                                        <?php the_post_thumbnail('avatar', array('class' => 'img-responsive')); ?>
                                    </a>
                                </div>
                                <div class="product-home-item-info col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <h3><?php the_title(); ?></h3>
                                    <?php echo $product->get_price_html(); ?>
                                </div>
                            </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>

                    <?php $args = array('post_type' => 'product', 'posts_per_page' => 5, 'order' => 'DESC', 'orderby' => 'date', 'post__not_in' => $array_posts, 'tax_query' => array( array( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => $product_cat->slug ))); ?>
                    <?php query_posts($args); ?>
                    <?php if (have_posts()) : ?>
                    <div class="product-home-item-container col-lg-6 col-md-6 col-sm-6 col-xs-12 no-paddingr">
                        <div class="product-home-item-container-wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingr">
                            <?php while (have_posts()) : the_post(); ?>
                            <?php array_push($array_posts, get_the_ID());  ?>
                            <?php $product = new WC_Product(get_the_ID()); ?>
                            <div class="product-home-item col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                <div class="product-home-item-img col-lg-4 col-md-4 col-sm-4 col-xs-4 no-paddingl no-paddingr">
                                    <?php the_post_thumbnail('avatar', array('class' => 'img-responsive')); ?>
                                </div>
                                <div class="product-home-item-info col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <h3><?php the_title(); ?></h3>
                                    <?php echo $product->get_price_html(); ?>
                                </div>
                            </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="product-home-featured-product-container col-lg-6 col-md-6 col-sm-6 col-xs-12 no-paddingl">
                        <?php $args = array('post_type' => 'product', 'posts_per_page' => 1, 'order' => 'DESC', 'orderby' => 'date', 'post__not_in' => $array_posts, 'tax_query' => array( array( 'taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => $product_cat->slug ))); ?>
                        <?php query_posts($args); ?>
                        <?php if (have_posts()) : ?>
                        <div class="product-home-featured-product-item col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                            <div class="product-home-featured-product-item-wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12  no-paddingl no-paddingr">
                                <?php while (have_posts()) : the_post(); ?>
                                <?php $product = new WC_Product(get_the_ID()); ?>
                                <?php array_push($array_posts, get_the_ID());  ?>
                                <div class="product-home-featured-item-bar col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <h2><?php _e('NUEVO PRODUCTO', 'keyma'); ?></h2>
                                </div>
                                <div class="product-home-item-img col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <?php the_post_thumbnail('product_img', array('class' => 'img-responsive')); ?>
                                </div>
                                <div class="product-home-item-info col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h3><?php the_title(); ?></h3>
                                    <div class="product-home-featured-price col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                        <?php echo $product->get_price_html(); ?>
                                    </div>
                                </div>
                                <button class="btn btn-md btn-cart"><?php _e('ver más', 'keyma'); ?></button>

                                <?php endwhile; ?>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
