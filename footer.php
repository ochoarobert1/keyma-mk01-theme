<footer class="container-fluid" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row">
        <div class="suscription-form-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="suscription-form-content col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <div class="col-md-8">
                            <h3>SUSCRÍBETE PARA RECIBIR OFERTAS Y <strong>NOVEDADES</strong></h3>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input type="email" class="form-control" placeholder="<?php _e('Ingrese su correo electrónico', 'keyma'); ?>">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-envelope"></i></button>
                                </span>
                            </div><!-- /input-group -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="the-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="footer-item col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-footer.png" alt="<?php echo get_bloginfo('name')?>" class="img-responsive" />

                    </div>
                    <div class="footer-item col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <h2><?php _e('Información', 'keyma'); ?></h2>
                        <div class="footer-data-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <p><?php echo get_option('keyma_dir'); ?></p>
                            <p><span><?php _e('Telefonos:', 'keyma' ); ?></span> <?php echo get_option('keyma_telf'); ?></p>
                            <p><span><?php _e('Correo:', 'keyma' ); ?></span> <?php echo get_option('keyma_email'); ?></p>

                            <div class="footer-data-container-icons col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <i class="fa fa-facebook"></i>
                                <i class="fa fa-twitter"></i>
                                <i class="fa fa-instagram"></i>
                            </div>

                        </div>
                    </div>
                    <div class="footer-item col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <h2><?php _e('enlaces', 'keyma'); ?></h2>
                        <?php wp_nav_menu(array('container_class' => 'menu-footer', 'theme_location' => 'footer_menu', 'items_wrap' => '<ul id="%1$s" class="%2$s footer-nav">%3$s</ul>' )); ?>
                    </div>
                    <div class="footer-item col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <h2><?php _e('categorias', 'keyma'); ?></h2>
                        <?php $args = array('taxonomy' => 'product_cat', 'parent' => 0); ?>
                        <?php $terms = get_terms($args); ?>
                        <?php foreach ($terms as $term) { ?>
                        <a href="<?php echo get_term_link($term); ?>" title="<?php echo $term->name; ?>" class="footer-cat-item"><?php echo $term->name; ?></a>
                        <?php } ?>
                    </div>

                    <div class="footer-copy col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <div class="footer-copy-info col-md-8 no-paddingl">
                            <h6>&copy; Keyma Corp. 2017 - <?php _e('Creado con', 'keyma'); ?> <i class="fa fa-heart"></i> <?php _e('por', 'keyma'); ?> <a href="http://robertochoa.com.ve">Robert Ochoa</a> - <?php _e('Todos los derechos reservados', 'keyma'); ?></h6>
                        </div>
                        <div class="footer-copy-img col-md-4 no-paddingr">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/cards-test.png" alt="<?php _e('Aceptamos los siguientes métodos', 'keyma'); ?>" class="img-responsive" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>
</html>
