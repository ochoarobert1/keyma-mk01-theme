<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <?php /* MAIN STUFF */ ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset') ?>" />
        <meta name="robots" content="NOODP, INDEX, FOLLOW" />
        <meta name="HandheldFriendly" content="True" />
        <meta name="MobileOptimized" content="320" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="dns-prefetch" href="//connect.facebook.net" />
        <link rel="dns-prefetch" href="//facebook.com" />
        <link rel="dns-prefetch" href="//googleads.g.doubleclick.net" />
        <link rel="dns-prefetch" href="//pagead2.googlesyndication.com" />
        <link rel="dns-prefetch" href="//google-analytics.com" />
        <?php /* FAVICONS */ ?>
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />
        <?php /* THEME NAVBAR COLOR */ ?>
        <meta name="msapplication-TileColor" content="#98CF00" />
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/win8-tile-icon.png" />
        <meta name="theme-color" content="#98CF00" />
        <?php /* AUTHOR INFORMATION */ ?>
        <meta name="language" content="<?php echo get_bloginfo('language'); ?>" />
        <meta name="author" content="Keyma" />
        <meta name="copyright" content="https://keyma.cl/" />
        <meta name="geo.position" content="-33.455112,-70.691192" />
        <meta name="ICBM" content="-33.455112,-70.691192" />
        <meta name="geo.region" content="CL" />
        <meta name="geo.placename" content="Av Libertador Bernardo O'Higgins 4050, Santiago, Estación Central, Región Metropolitana, Chile" />
        <meta name="DC.title" content="<?php if (is_home()) { echo get_bloginfo('name') . ' | ' . get_bloginfo('description'); } else { echo get_the_title() . ' | ' . get_bloginfo('name'); } ?>" />
        <?php /* MAIN TITLE - CALL HEADER MAIN FUNCTIONS */ ?>
        <?php wp_title('|', false, 'right'); ?>
        <?php wp_head() ?>
        <?php /* OPEN GRAPHS INFO - COMMENTS SCRIPTS */ ?>
        <?php get_template_part('includes/header-oginfo'); ?>
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php /* IE COMPATIBILITIES */ ?>
        <!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7" /><![endif]-->
        <!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8" /><![endif]-->
        <!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9" /><![endif]-->
        <!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js" /><!--<![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script> <![endif]-->
        <!--[if IE]> <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" /> <![endif]-->
        <?php get_template_part('includes/fb-script'); ?>
        <?php get_template_part('includes/ga-script'); ?>
    </head>
    <body class="the-main-body <?php echo join(' ', get_body_class()); ?>" itemscope itemtype="http://schema.org/WebPage">
        <div id="fb-root"></div>
        <header class="container-fluid" role="banner" itemscope itemtype="http://schema.org/WPHeader">
            <div class="row">
                <div id="sticker" class="the-header col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <div class="the-header-desktop col-lg-12 col-md-12 hidden-sm hidden-xs no-paddingl no-paddingr">
                        <div class="header-logo col-lg-2 col-md-2 col-sm-3 col-xs-3">
                            <a href="<?php echo home_url('/'); ?>" title="<?php _e('Volver al Inicio', 'keyma'); ?>">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="<?php echo get_bloginfo('name'); ?>" class="img-responsive img-logo" />
                            </a>
                        </div>
                        <div class="header-main-container col-lg-10 col-md-10 col-sm-9 col-xs-3 no-paddingl no-paddingr">
                            <div class="header-small-bar col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                <div><i class="fa fa-map-marker"></i> <?php echo get_option('keyma_dir'); ?></div>
                                <div><i class="fa fa-phone"></i> <?php echo get_option('keyma_telf'); ?></div>
                                <div><i class="fa fa-mobile"></i> <?php echo get_option('keyma_mob'); ?></div>
                                <div><i class="fa fa-envelope"></i> <a href="mailto:<?php echo get_option('keyma_email'); ?>"><?php echo get_option('keyma_email'); ?></a></div>
                                <div class="search-bar pull-right"> <i class="fa fa-search"></i></div>
                                <?php global $woocommerce; ?>
                                <?php /* MY ACCOUNT PAGE */ ?>
                                <?php $myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' ); ?>
                                <?php if ( $myaccount_page_id ) { $myaccount_page_url = get_permalink( $myaccount_page_id ); } ?>
                                <div class="account-bar pull-right"><i class="fa fa-user-circle-o"></i> <a href="<?php echo esc_url($myaccount_page_url); ?>" title="<?php _e('MI CUENTA', 'keyma'); ?>"><?php _e('MI CUENTA', 'keyma'); ?></a></div>
                                <?php /* SHOPPING CART PAGE */ ?>
                                <?php $cart_url = $woocommerce->cart->get_cart_url(); ?>
                                <div class="cart-bar pull-right"><i class="fa fa-shopping-cart"></i> <a href="<?php echo esc_url($cart_url); ?>" title="<?php _e('CARRITO', 'keyma'); ?>"><?php _e('CARRITO', 'keyma'); ?></a></div>
                            </div>
                            <nav class="header-main-bar col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                <div class="custom-big-navbar-container">
                                    <?php $theme_locations = get_nav_menu_locations(); ?>
                                    <?php $menu_obj = get_term( $theme_locations['header_menu'] ); ?>
                                    <?php echo special_menu_container($menu_obj->name); ?>

                                </div>
                            </nav>
                        </div>
                    </div>
                    <div class="the-header-mobile hidden-lg hidden-md col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <nav class="navbar navbar-default" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
                            <div class="container-fluid">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>" itemscope itemtype="http://schema.org/Organization">
                                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="<?php echo get_bloginfo('name'); ?>" class="img-responsive img-logo" />
                                    </a>
                                </div>

                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <?php wp_nav_menu( array( 'theme_location' => 'header_menu', 'depth' => 2, 'container' => 'div',
                                                         'container_class' => 'collapse navbar-collapse', 'container_id' => 'bs-example-navbar-collapse-1', 'menu_class' => 'nav navbar-nav', 'fallback_cb' => 'wp_bootstrap_navwalker::fallback', 'walker' => new wp_bootstrap_navwalker() )); ?>
                            </div><!-- /.container-fluid -->
                        </nav>
                    </div>
                </div>
            </div>
        </header>
